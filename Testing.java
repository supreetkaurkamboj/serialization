/*A program to demonstration serialization for various different dependencies */

import java.io.*;
class abc implements Serializable
{
    String city;
    int code;
    abc(String city, int code)
       {
         this.city = city;
         this.code = code;  
       } 
}
class def implements Serializable
{
    int id;
    String name;
    def(int id, String name)
      {
          this.id=id;
          this.name=name;
      }
}
class Demo extends abc 
{
  int a;
  int b;
  def D; 
  public Demo(String city, int code, int a, int b, int id, String name)
    {
       super(city,code);
       D= new def(id,name);  
       this.a=a;
       this.b=b;
    }
  
}
class Testing
{
  public static void main(String[] args)
   {
      Demo obj = new Demo("sre",247001,1,2,12,"supreet");
      
      try
          {
             FileOutputStream file = new FileOutputStream("file.txt");
             ObjectOutputStream out = new ObjectOutputStream(file); 
             out.writeObject(obj);
             out.close();
             file.close();
             System.out.println("object has been serialized");
        }
     catch(IOException ex)
        {
             System.out.println("IO Exception is caught"+ex);
        }
     try
        {
            FileInputStream file = new FileInputStream("file.txt");
            ObjectInputStream in = new ObjectInputStream(file);
            obj = (Demo)in.readObject();
            in.close();
            file.close();
            System.out.println("Object has been deserialized");
            System.out.println("city =" + obj.city);
            System.out.println("code =" + obj.code);
            System.out.println("a=" + obj.a);
            System.out.println("b=" + obj.b);
            System.out.println("id =" + obj.D.id);
            System.out.println("name=" + obj.D.name);

        }
catch(IOException ex)
        {
             System.out.println("IO Exception is caught"+ex);
        }
catch(ClassNotFoundException ex)
        {
              System.out.println("ClassNotFoundException caught");
        }
    }
}